package threads

import (
	"github.com/bwmarrin/discordgo"
)

func ThreadController(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.ChannelID == "812730403894722560" {
		if ch, err := s.State.Channel(m.ChannelID); err != nil || !ch.IsThread() {
			thread, err := s.MessageThreadStartComplex(m.ChannelID, m.ID, &discordgo.ThreadStart{
				Name:                m.Author.Username + " Опять всё сломал",
				AutoArchiveDuration: 60,
				Invitable:           false,
				RateLimitPerUser:    10,
			})
			if err != nil {
				panic(err)
			}
			_, _ = s.ChannelMessageSend(thread.ID, "Это всё из-за твоих платных модов")
			m.ChannelID = thread.ID
		} else {
			_, _ = s.ChannelMessageSendReply(m.ChannelID, "Это всё из-за твоих платных модов", m.Reference())
		}
	}
}
