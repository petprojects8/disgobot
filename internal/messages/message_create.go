package messages

import (
	"DisGoBot/internal/generate"
	"github.com/bwmarrin/discordgo"
)

func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	Thief := generate.GenerateThiefs()
	Action := generate.GenerateActions()
	ModOwner := generate.GenerateModOwners()
	ModType := generate.GenerateModTypes()
	// Отправка сообщения
	if m.Author.ID == s.State.User.ID {
		return
	}
	if m.Content == ";спиздил" {
		s.ChannelMessageSend(m.ChannelID, "час назад "+Thief+" "+Action+" "+ModType+" у "+ModOwner)
	}
}
