package generate

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

func GenerateThiefs() string {
	ThiefFile, err := ioutil.ReadFile("Thief")
	if err != nil {
		fmt.Println("Введи воров")
		return ""
	}
	Thiefs := strings.Split(string(ThiefFile), ";")

	s := rand.NewSource(time.Now().Unix() + 3)
	r := rand.New(s)
	Thief := fmt.Sprint(Thiefs[r.Intn(len(Thiefs))])
	return Thief
}
func GenerateActions() string {
	// Генерация Действия
	ActionFile, err := ioutil.ReadFile("Action")
	if err != nil {
		fmt.Println("Введи действия")
		return ""
	}
	Actions := strings.Split(string(ActionFile), ";")

	s := rand.NewSource(time.Now().Unix() + 3)
	r := rand.New(s)
	Action := fmt.Sprint(Actions[r.Intn(len(Actions))])
	return Action
}
func GenerateModTypes() string {
	// Генерация Мода
	ModTypeFile, err := ioutil.ReadFile("ModType")
	if err != nil {
		fmt.Println("Введи моды")
		return ""
	}
	ModTypes := strings.Split(string(ModTypeFile), ";")

	s := rand.NewSource(time.Now().Unix() + 3)
	r := rand.New(s)
	ModType := fmt.Sprint(ModTypes[r.Intn(len(ModTypes))])
	return ModType
}
func GenerateModOwners() string {
	// Генерация пидораса
	ModOwnerFile, err := ioutil.ReadFile("ModOwner")
	if err != nil {
		fmt.Println("Введи пидоров")
		return ""
	}
	ModOwners := strings.Split(string(ModOwnerFile), ";")

	s := rand.NewSource(time.Now().Unix() + 3)
	r := rand.New(s)
	ModOwner := fmt.Sprint(ModOwners[r.Intn(len(ModOwners))])
	return ModOwner
}
